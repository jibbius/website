var fs = require('fs');
let jsonFile = fs.readFileSync('./data/MOCK_DATA.json', 'utf-8')
let jsonMock = JSON.parse(jsonFile);

const DB = require('better-sqlite3-helper');

DB({
    path: './data/sqlitedb.db', // this is the default
    memory: false, // create a db only in memory
    readonly: false, // read only
    fileMustExist: true, // throw error if database not exists
    WAL: true, // automatically enable 'PRAGMA journal_mode = WAL'
    migrate: {  // disable completely by setting `migrate: false`
      force: false, // set to 'last' to automatically reapply the last migration-file
      table: 'migration', // name of the database table that is used to keep track
      migrationsPath: './migrations' // path of the migration-files
    }
  }) 

console.log("Hello! we're about to populate the data in here. we'll use MOCK_DATA.json");

console.log(`About to insert ${ Object.keys(jsonMock).length } entries..`);
  jsonMock.forEach(element => { 
      DB().insert('parts', {
        "data": JSON.stringify(element)
    });
     
  })
console.log(`Success! Finished.`);

