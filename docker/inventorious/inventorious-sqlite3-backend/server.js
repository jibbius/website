'use strict';

const DB = require('better-sqlite3-helper');
var express = require('express')
var app = express();
const cors = require('cors');
const path = require('path');
const config = require('config');
const shortid = require('shortid');

// Don't forget to set your environment the first time! For example export NODE_ENV=development , 
// Which will use config/development.json on top of config/production.json

const port = config.get('Server.settings.port');
const host = config.get('Server.settings.hostname');

app.use( "/hi", express.json() );
app.use( "/parts", [cors(), express.json()] );

DB({
    path: './data/sqlitedb.db', // this is the default
    memory: false, // create a db only in memory
    readonly: false, // read only
    fileMustExist: false, // throw error if database not exists
    WAL: true, // automatically enable 'PRAGMA journal_mode = WAL'
    migrate: {  // disable completely by setting `migrate: false`
      force: false, // set to 'last' to automatically reapply the last migration-file
      table: 'migration', // name of the database table that is used to keep track
      migrationsPath: './migrations' // path of the migration-files
    }
  }) 

// let row = DB().queryFirstRow('SELECT * FROM users WHERE id=?', userId);

app.post('/hi', function(request, response){
  console.log(request.body);      // your JSON
   response.send(request.body);    // echo the result back
   console.log(request.body.name);
});

app.post('/parts', function(request, response){
  //console.log(request.body);      // your JSON
  let resultStatus = 400;
  let newBody = request.body;

  // Has the user tried to give us an ID? Attempt to use that.
  if (typeof request.body.id !== 'undefined') {

    // Lets verify the ID is going to be unique first.
    let query = DB().query('SELECT * FROM parts');
    let allParts = query.map(row => {
        // We only want the JSON data
        return JSON.parse(row.data);
    })

    const hasMatch = allParts.find(function(element) {
      return element.id === request.body.id;
    })

    if (typeof hasMatch === 'undefined') {
      // No Previous Duplicate ID, Lets go insert it (nice and easy)
      console.log(`Parts Add: Thanks, adding it in with the ID you gave (${request.body.id}).`)
      DB().insert('parts', {
        "data": JSON.stringify(newBody)
      });
      
      resultStatus = 200;
    } else {
      // We have an ID, but it's a duplicate. generate a fresh one.
      newBody.id = shortid.generate();
      console.log(`Parts Add: Duplicate ID problem, making a new one: ${newBody.id}`)
        DB().insert('parts', {
          "data": JSON.stringify(newBody)
      });
      
      resultStatus = 200;
      // Should allow for iterating over a list if it is multiple items
    } 
    
  }
   
  // All good? all good. We hope.
  response.status = resultStatus;

  // We should perhaps return the next available ID and use that?
  response.send(newBody);    // echo the result back
});

app.get('/parts', function(request, response){

  let query = DB().query('SELECT * FROM parts');
  let allParts = query.map(row => {
      return JSON.parse(row.data);
  })

  // If we send a 304 then we don't process the code latter...
  response.statusCode = 200;
  response.send(allParts);
});
//console.log(allParts);

// If the user wants to also serve the front end itself, let's serve that folder too. 
// You would do this with node server.js includefrontend
if (process.argv[2] === "includefrontend") {
  app.use(express.static('frontend'));
  /* Enable this to fall back to the react router if it can't find an address. 
  This is probably not what you want if you are hosting it with other applications
  */
  // app.get('*', (req,res) =>{
  //   // res.send(`it didn't work ${__dirname}`);
  //   res.sendFile(path.join(__dirname+'/frontend/index.html'));
  // });
  // Selectively adding it to just the endpoints I am using at the moment.
  app.get(['/add', '/about'], (req,res) =>{
    // res.send(`it didn't work ${__dirname}`);
    res.sendFile(path.join(__dirname+'/frontend/index.html'));
  });  

  
  app.get('*', (req,res) =>{
    // res.send(`it didn't work ${__dirname}`);
    res.sendFile(path.join(__dirname+'/frontend/index.html'));
  });  

  console.log(`Optional: We are serving the front end on http://${host}:${port}/`);
}


console.log(`Now listening on port http://${host}:${port}/parts`);
app.listen(port, host);
