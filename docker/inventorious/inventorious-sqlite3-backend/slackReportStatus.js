// Lets check for new items.
const DB = require('better-sqlite3-helper');
var config = require('config');

DB({
    path: './data/sqlitedb.db', // this is the default
    memory: false, // create a db only in memory
    readonly: false, // read only
    fileMustExist: true, // throw error if database not exists
    WAL: true, // automatically enable 'PRAGMA journal_mode = WAL'
    migrate: {  // disable completely by setting `migrate: false`
      force: false, // set to 'last' to automatically reapply the last migration-file
      table: 'migration', // name of the database table that is used to keep track
      migrationsPath: './migrations' // path of the migration-files
    }
}) 

// Lets get our list of items
// The current week

//query = "SELECT * FROM parts WHERE DATE(created) >= DATE('now', 'weekday 0', '-7 days')";

// Within 7 days of it running
// let lookBackDays = 7;
// const query = "SELECT * FROM parts WHERE created > (SELECT DATETIME('now', '-? day'))";

const query = "SELECT * FROM parts WHERE created > (SELECT DATETIME('now', '-7 day'))";

// Test selecting only one
// const query = "SELECT * FROM parts WHERE created > (SELECT DATETIME('now', '-7 day')) LIMIT 1";
// const query = "SELECT data FROM parts";

  let allParts = DB().query(query);  
  
  var partsToShow = allParts.filter(function (itemRow) {
    let item = JSON.parse(itemRow.data);
    return (item.stockLevel).toString() !== "0";
  });

console.log(`After checking and finding ${allParts.length} parts, there are ${partsToShow.length} that are in stock.`);


// Ok, we have some, lets go ahead
if (partsToShow.length > 0) {
    console.log(`We have found ${partsToShow.length} new part(s) in stock. Sending a message to Slack.`)
    
    let pluralised = ((partsToShow.length.toString() ) == "1") 
    ? `is one new thing` 
    : `are *${partsToShow.length}* new things`;

    let headingMessage = `Hello! beep boop. \nThere ${pluralised} added to the space in the past week. _Woohoo!_`
    
    let partsList = "";

    if ( (partsToShow.length) > 1) {
    // More than one / Multiple items to show.
    partsList = "\nand here they are! ";
    partsList += partsToShow.map((itemRow) => {
        let item = JSON.parse(itemRow.data)
        return `\n${item.name} - _(${item.stockLevel})_`
    })
    } else {
        // console.log(partsToShow);
        let item = JSON.parse(partsToShow[0].data)
        partsList += `_spoiler alert_, it's an *${item.name}*`
    }

    let endingMessage = `\nIf you have a request for a part or thing at the space, please let someone on the committee know or ask in <#C02HUMNV8|space>.`

    // console.log(partsList);
    //Message has been generated, now to send it to slack.

    var Slack = require('slack-node');

    // These are grabbed from our config/default.json , as well as the name of your environment (ie make a development.json and put your secrets in there!)
    var webhookUri = config.get('Slack.webhook');

    var channel = config.get('Slack.channel');
    var username = config.get('Slack.username');
    var icon_emoji = config.get('Slack.icon_emoji');

    if (webhookUri === "YOUR_WEBHOOK_URL") {
        console.log("To Post to slack, you will need to get a webhook URL you can post to.")
        console.log("Once you have one of them, add it to the proper .json in /config and have at it")
        console.log("https://api.slack.com/incoming-webhooks for more information. Quitting for now")
        process.exit(0) 
    }

    slack = new Slack();
    slack.setWebhook(webhookUri);
 
    slack.webhook({
    channel,
    username,
    icon_emoji,
    text: `${headingMessage} ${partsList} ${endingMessage}`,
    mrkdwn: true
    }, function(err, response) {
    console.log(response);
    });

} else {
    console.log("There are no entries in the database found. ")
}

