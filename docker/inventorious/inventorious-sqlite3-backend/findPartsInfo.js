const DB = require('better-sqlite3-helper');

DB({
    path: './data/sqlitedb.db', // this is the default
    memory: false, // create a db only in memory
    readonly: false, // read only
    fileMustExist: true, // throw error if database not exists
    WAL: true, // automatically enable 'PRAGMA journal_mode = WAL'
    migrate: false
  }) 


let row = DB().queryFirstRow('SELECT * FROM parts');
console.log(row.data);
