import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/AddCircle';
import { IconButton } from '@material-ui/core';

class FormDialog extends React.Component{
  constructor(props){
    super(props);
    this.state = ({
      open: false,
      location: ""
    })
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  handleDialogUpdate = (value) => {
    this.setState({
      location: value.target.value,
    });
  };  

  addOption = () => {
    this.props.callbackFromParent(this.state.location)
    this.handleClose();
  }  


    render() {
        return(
     <div>
       <IconButton title="Click to add a new location" style={{marginRight:"8px", marginTop:"24px"}} onClick={this.handleClickOpen}>
         <AddIcon></AddIcon>
       </IconButton>
       <div>               
       <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
         <DialogTitle id="form-dialog-title">Add new location</DialogTitle>
         <DialogContent>
           <DialogContentText>
             Type in a new location to add it to the option list. Once you have added a part, it will stay there forever.
           </DialogContentText>
           <TextField
            onChange={(value) => this.handleDialogUpdate(value) }
             autoFocus
             margin="dense"
             id="partLocation"
             label="Part Location"
             type="text"
             fullWidth
           />
         </DialogContent>
         <DialogActions>
           <Button onClick={this.handleClose} color="primary">
             Cancel
           </Button>
           <Button onClick={this.addOption} color="primary">
             Add location
           </Button>
         </DialogActions>
       </Dialog>
     </div>
    </div>
      )
    }
};

export default FormDialog;