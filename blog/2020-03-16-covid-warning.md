---
author: Committee
feature: /images/blog/covid19.png
title: Hackerspace policy for managing COVID-19
tags: covid-19, coronavirus
---

G'day hackers,

**TL; DR**

We will be closing the space for at least the next two weeks, reassessing at the end of the month if another two week closure is required. We will be crediting all existing memberships with the exception of full time members who continue to use the space in their own time.

**Long Version;**

No doubt you have been watching the development of COVID-19 and you may even be wondering what policy the Hackerspace has in place to manage the situation in a local context. Or you may just be wondering where you can get some toilet paper.

The Hackerspace will be following advice supplied by recognised authorities such as The World Health Organisation, The Australian Department of Health and The Victorian Department of Health and Human Services. We request that all members take reasonable care for their own health and safety and to also consider the health of all those within our community by adhering to advice offered through these official channels.

With that in mind and with the context of the Victorian state government just declared a state of emergency, we will be closing the space for at least the next two weeks. We will reassess at the end of the month, and unless conditions have significantly improved will likely close for a further two weeks before reassessing again.

What does this mean?

* Our advertised open times (Mon, Tue, Wed and Sat) will be closed for the next few weeks.
* Social members who want to collect their tubs can do so this evening (16 Mar) between 6pm and 7.30pm, or alternatively arranged with a committee member at a later date.
* Social members will be credited for the full duration the space is closed due to COVID-19.
* Full time members, who opt-in to not use the space for the duration of the closure will be credited for that duration also.
* Full time members who choose to continue to use the space at their own discretion and will not be credited (also no partial credits).
* The Hackerspace committee will continue to monitor official sources and will notify when our normal operating hours resume. If you have any concerns please do not hesitate to speak with the committee.

Stay safe and be excellent to each other.

Committee.

You can find out more about the novel COVID-19 virus below:

* [Australian department of health's website](https://www.health.gov.au/news/health-alerts/novel-coronavirus-2019-ncov-health-alert#current-status)
* [World Health Organisation](https://www.who.int/health-topics/coronavirus])
* [A simulation of how the virus may play out in the coming weeks depending on social changes](https://www.washingtonpost.com/graphics/2020/world/corona-simulator/)
