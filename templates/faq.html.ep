% layout 'page', active_page => 'faq', page_title => 'FAQ';

<section class="section">
  <div class="container">
    <h2 class="title text-center">Frequently Asked Questions</h2>

    <h3>What exactly do you do?</h3>
    <p>The Ballarat Hackerspace is an organisation that promotes learning STEAM (Science, Technology, Engineering, Arts and Mathematics) through a "hacker culture" - that is, learn to build, fix, make and do things yourself, and build those skills dealing with modern and future technologies. We mainly run a workshop that has tools, equipment and space for doing this learning, and promote a healthy community of like minded individuals in the Ballarat region in Victoria, Australia.""

    <h3>What equipment do you have?</h3>
    <p>We have 3D printers, a CNC machine, soldering stations, electronics, Raspberry Pis setup, computers, testing equipment, cleaning equipment, a kettle, specialised software for 3D modelling, and a LASER CUTTER.</p>

    <h3>How do I get in contact</h3>
    <p>We welcome you to come in and have a chat in person, but you are also welcome to contact us through any of the means on our <a href='/contact'>Contact</a> page.</p>


    <h2>Join us</h2>


    <h3>This sounds great - how can I help?</h3>
    <p>Join us by becoming a member, either casually or a full time member. Check out the <a href="/join">Join</a> page for more information.</p>

    <p>We are looking for sponsorships from organisations that share our goals. Money from sponsorship goes into increasing workshops, new tools, and building an ever bigger hackerspace.</p>
    <p>Individuals can help by becoming a member and participating in the community!</p>

    <h3>Do you allow younger people to join?</h3>
    <p>We do allow younger people to join, but please keep in mind that we do not do guided lessons (outside of workshops). If your child wants to participate, they are more than welcome, but we require that are parent comes with them to the space as their guardian, who is fully responsible for that child. Full memberships allow an adult to bring a child with them to the space (several of our members bring children), on the understanding that children are looked after by their parent. </p>
    <p>Members are responsible for their own safety when in the space, even younger members.</p>



    <h3>What if I don't have a project?</h3>
    <p>We have projects and things that could be done around the space. This isn't busy work either - we need stuff built, and you'll learn lots on the way. Contact us for ideas.</p>


    <h3>I'm just a beginner - can I use the space?</h3>
    <p>Definitely! Many of our members had a low understanding of what they were doing when they started - the Ballarat Hackerspace is <i>for learning</i>, and we have lots of helpful people around that can give you pointers and help out. Keep in mind, though, that the space is unstructured - you'll be responsible for your learning most of the time.</p>
    <p>If you want to use equipment you don't know how to use, we will be doing some inductions from time to time on usage.</p>

    <h3>I don't have time to come into the hackerspace much. What can I do?</h3>
    <p>If you like what we are doing, but can't come in, you can sponsor us from our Patreon page. In addition, several people like coming to workshops, as they are fixed times and have a fixed length (making it easy to ask your parents to babysit your kids for the workshop). Feel free to sign up to those.</p>


    <h2>Using the space</h2>

    <h3>When are you open?</h3>
    <p>We are open Monday evening (6pm-9pm), Tuesday evening (4pm - 9pm) and Saturdays (9am - 3pm) to the general public. More information is on our <a href="/about">About page</a>. Full time members also have 24/7 access to the space. You are welcome to come in an have a chat if a member is at the space and the door is open.</p>

    <h3>Can I come see the space?</h3>
    <p>Yes. Even if you aren't a member, feel free to drop in during our opening times to see the space and have a chat.</p>

    <h3>Is the space open today?</h3>
    <p>The space opens on "best effort" during our listed opening times, and there is a small chance it may not open. Check out our twitter page, or contact us directly to confirm a time if you are planning to come in for the first time. We are <i>almost always</i> open during those times, but occasional hiccups will happen where no volunteer is available. We post these messages to slack if we know in advance this will be happening.</p>

    <h3>Can I book the space out for an event or meeting?</h3>
    <p>No. We don't allow people to book out the space exclusively. Full time members are welcome to come in at different times, and you can have people visit (not using equipment!), but keep in mind that others may be using the space, including using noisey equipment.</p>


    <h3>Can I book out the 3D printer? (or laser cutter, etc)</h3>
    <p>Members can use equipment on a <i>fair use</i> basis. This normally results in "first in, best dressed", but they are shared resources and the committee members may kick you off a machine if you are using it too much, especially when others are looking to use the equipment.<p>
    <p>Note also we do not guarantee that equipment is <i>even working</i>. Machines may be broken, in the process of being upgraded, or not right for your job. We may run out of filament in the colour you are after. We make no guarantees regarding this. The primary function of the workshop is as a learning facility, not as a print-for-hire business.<p>
    <p>If you are looking for a guaranteed and bookable facility, checkout local companies such as the <a href="https://runwayhq.co/runway-ballarat-2/fab-lab/">Runway Fab Lab</a> or other engineering and manufacturing firms. We highly recommend this option if you are looking to do commerical 3d printing, laser cutting, etc.</p>


    <h3>Can I store stuff at the space?</h3>
    <p>Members get a tub for project storage. For larger storage needs, the answer is normally "no", but talk to us first.</p>


    <h2>Workshops</h2>

    <h3>Do you do guided workshops?</h3>
    <p>We are mainly an area for self-learning and exploration, however we do workshops on specific areas from time to time. Head to our <a href="/workshops">Workshops</a> page for information on the current workshops planned and in progress.</p>

    <p>If you need training on a machine, ask a committee member who is in the space. We'll either train you up, or organise another time to do so.</p>

    <h3>Can you do a workshop on X?</h3>

    <p>Maybe! We would be interested in hearing your suggestions for new workshops, but keep in mind we are volunteer run - workshops are done when someone has time and is interested in the idea.</p>


    <h2>Member Benefits</h2>
    <h3>Do members get discounts on stuff?</h3>
    <p>Usually not. Our memberships are <i>very cheap</i> compared to the market in this space, and serve to ensure we cover costs and not much more (we are non-profit after all). Workshops and equipment are sold at near-cost prices too (we can show you the spreadsheets if you want), so there isn't much of a discount to give.</p>

    <p>If you do want to learn something, but don't have the money to get it, then have a chat to a committee member. It may be possible (but not guaranteed) that we could buy it for the space, and you can use it within that context. For example, our laser cutter.</p>

    
    <h2>Meta information</h2>

    <h3>Isn't hacking illegal?</h3>
    <p>We don't do hacking in the "computer security" sense, which is breaking into computer systems. We use the word "hack" in its original sense - to break something open, change how it works, and make it do new things. A similar phrase to hack/hacker/hackerspace in this space is make/maker/makerspace.</p>
    <p>The most "illegal" thing we do (which isn't really illegal) is voiding warranties.</p>

    <h3>Are you part of Federation University?</h3>
    <p>We are not part of FedUni at all, except for our location is on-campus. FedUni sponsor the space, but we are otherwise completely independent from them.</p>

    <h3>How are you funded?</h3>
    <p>We are funded by our members, and also by our gracious sponsors. Our workshops are run at near-cost price.</p>


    <h3>Can you build something for us?</h3>
    <p>Members are welcome to do work, including paid work, using the resources of the hackerspace (as long as this otherwise fits in our agreement). In that light, you are welcome to make a specific agreement with <i>members of the space</i>, but the hackerspace has no interest or liability in that agreement.</p>



  </div>
</section>
