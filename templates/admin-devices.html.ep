% layout 'page', active_page => 'admin', page_title => 'Devices';

<div ng-controller="AdminDevicesController as adminDevicesVm">
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="title text-center"><a href="/admin">Admin</a> <i class="fa fa-angle-double-right"></i> Devices</h2>
          <p>Manage devices that have privileged access to particular items on the 'bhack' network.</p>
          <p>To ensure the best experience whilst at the Ballarat Hackerspace members should ensure their devices are registered.</p>

          <div class="text-right">
            <button class="btn btn-cta btn-cta-primary" data-toggle="modal" ng-click="adminDevicesVm.addDevice()"><i class="fa fa-fw fa-plus"></i> Register new Device</button>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h3>Registered Devices</h3>
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="hidden-xs hidden-sm hidden-md">Owner</th>
                <th>Name</th>
                <th>UUID</th>
                <th>Token</th>
                <th>Roles</th>
                <th>Registered</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="d in adminDevicesVm.devices" ng-class="{'success': adminDevicesVm.isDeviceCurrent(d)}">
                <td class="hidden-xs hidden-sm hidden-md">{{d.email}}</td>
                <td>{{d.name}}</td>
                <td>{{d.uuid.substr(0, 8)}}...</td>
                <td>{{d.token.substr(0, 8)}}...</td>
                <td>{{adminDevicesVm.formatRole(d.roles)}}</td>
                <td><bh-time format="YYYY-MM-DD HH:mm:ss" epoch="{{d.created}}"></bh-time></td>
                <td class="text-right">
                  <button class="btn btn-cta btn-cta-sm btn-cta-blue" ng-click="adminDevicesVm.editDevice(d)"><i class="fa fa-fw fa-pencil-alt"></i></button>
                  <button class="btn btn-cta btn-cta-sm btn-cta-red" ng-click="adminDevicesVm.deleteDevice(d)"><i class="fa fa-fw fa-times"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- device add modal -->
<script type="text/ng-template" id="modalDeviceAdd.html">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="fa fa-times"></i></button>
    <h4 class="modal-title">Devices <i class="fa fa-angle-right"></i> Register</h4>
  </div>
  <div class="modal-body">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-tag"></i></span>
        <input name="uuid" type="text" class="form-control" placeholder="UUID" ng-model="modalVm.device.uuid" readonly>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
        <input name="token" type="text" class="form-control" placeholder="Token" ng-model="modalVm.device.token" readonly>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
        <input name="email" type="text" class="form-control" placeholder="Email" ng-model="modalVm.device.email">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-laptop"></i></span>
        <input name="device" type="text" class="form-control" placeholder="Device Name" ng-model="modalVm.device.name">
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta btn-cta-primary" type="button" ng-click="modalVm.ok()" ng-disabled="!modalVm.canAdd()"><i class="fa fa-plus"></i> Register</button>
    <button class="btn btn-cta pull-left" type="button" ng-click="modalVm.cancel()">Cancel</button>
  </div>
</script>

<!-- device edit modal -->
<script type="text/ng-template" id="modalDeviceEdit.html">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i aria-hidden="true" class="fa fa-times"></i></button>
    <h4 class="modal-title">Devices <i class="fa fa-angle-right"></i> Edit <i class="fa fa-angle-right"></i> {{modalVm.device.name}}</h4>
  </div>
  <div class="modal-body">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-tag"></i></span>
        <input name="uuid" type="text" class="form-control" placeholder="UUID" ng-model="modalVm.device.uuid" readonly>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-key"></i></span>
        <input name="token" type="text" class="form-control" placeholder="Token" ng-model="modalVm.device.token" readonly>
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
        <input name="email" type="text" class="form-control" placeholder="Email" ng-model="modalVm.device.email">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-laptop"></i></span>
        <input name="device" type="text" class="form-control" placeholder="Device Name" ng-model="modalVm.device.name">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-fw fa-list"></i></span>
        <select name="roles" type="text" class="form-control" placeholder="Roles" ng-model="modalVm.device.roles">
          <option ng-value="1">User</option>
          <option ng-value="32768">Super User</option>
        </select>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta btn-cta-green" type="button" ng-click="modalVm.ok()" ng-disabled="!modalVm.canSave()"><i class="fa fa-save"></i> Save</button>
    <button class="btn btn-cta pull-left" type="button" ng-click="modalVm.cancel()">Cancel</button>
  </div>
</script>

<!-- device delete modal -->
<script type="text/ng-template" id="modalDeviceDelete.html">
  <div class="modal-header">
    <h4 class="modal-title">Device <i class="fa fa-angle-right"></i> Delete</h4>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete the following registered device:</p>
    <ul><li>{{modalVm.device.name}} ({{modalVm.device.uuid}})</li></ul>
  </div>
  <div class="modal-footer">
    <button class="btn btn-cta" type="button" ng-click="modalVm.cancel()">Cancel</button>
    <button class="btn btn-cta btn-cta-red pull-left" type="button" ng-click="modalVm.ok()">Yes, Delete</button>
  </div>
</script>

<script>
% use Mojo::JSON 'encode_json';
% use Mojo::Util 'decode';
  window.bhack={
    member:<%== decode 'UTF-8', encode_json $member %>,
    devices:<%== decode 'UTF-8', encode_json $devices %>
  };
</script>
