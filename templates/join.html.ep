% layout 'page', active_page => 'join', page_title => 'Join';

<!-- join -->
<section id="join" class="section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="title text-center">Join</h2>
        <p>We have a number of member packages available for you to choose from. As a non-profit, all our membership fees are invested directly back into to provide new tools, maintaining exsiting tools and maintaining an inventory of parts and pieces for your projects.</p>

        <div class="row">
          <div class="col-sm-12 text-center">
            <div class="equal-panels">

              <div class="membership">
                <div class="panel panel-membership">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user-graduate"></i> Learner</h3>
                  </div>
                  <div class="panel-body">
                    <div class="price">$5+<span>/month</span></div>
                    <ul>
                      <li>Published open times access only.</li>
                      <li>Educational use of tools and consumables.</li>
                      <li>Help and learn from other members.</li>
                      <li>Find inspiration for a project of your own!.</li>
                      <li>A great way to support the Ballarat Hackerspace while
                      looking for a project to work on.</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a href="https://www.patreon.com/ballarathackerspace" class="btn btn-cta btn-cta-green">Join</a>
                  </div>
                </div>
              </div>

              <div class="membership">
                <div class="panel panel-membership">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-users"></i> Social</h3>
                  </div>
                  <div class="panel-body">
                    <div class="price">$30<span>/month</span></div>
                    <ul>
                      <li>All the benefits of the Learner Membership.</li>
                      <li>Use of tools and consumables on personal projects.</li>
                      <li>Small dedicated storage for projects.</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a href="https://ballarathackerspace.tidyhq.com/public/membership_levels/6IcnsQ" class="btn btn-cta btn-cta-green">Join</a>
                  </div>
                </div>
              </div>

              <div class="membership">
                <div class="panel panel-membership panel-membership-best">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-user-ninja"></i> Full Time</h3>
                  </div>
                  <div class="panel-body">
                    <div class="price">$60<span>/month</span></div>
                    <ul>
                      <li>All the benefits of the Social Membership.</li>
                      <li>24/7 access.</li>
                      <li>Large dedicated storage for projects.</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a href="https://ballarathackerspace.tidyhq.com/public/membership_levels/rDZmag" class="btn btn-cta btn-cta-green">Join</a>
                  </div>
                </div>
              </div>

              <div class="membership">
                <div class="panel panel-membership">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-seedling"></i> Seeder</h3>
                  </div>
                  <div class="panel-body">
                    <div class="price">$1024<span>/year</span></div>
                    <ul>
                      <li>All the benefits of the Full Time Membership.</li>
                      <li>Discounted workshops.</li>
                      <li>Sponsorship recognition.</li>
                      <li>Association membership included.</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a href="https://ballarathackerspace.tidyhq.com/public/membership_levels/lmDcyg" class="btn btn-cta btn-cta-green">Join</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <h3>How to become a Member</h3>
        <p>Joining is really, really, easy ...</p>
        <ol>
          <li>Select a membership above and click "Join".</li>
          <li>Check out the rules of the Hackerspace (including conditions of membership) and make sure that you agree to abide by them. That’s important for everyone’s safety and enjoyment.</li>
          <li><p>If not paying by Credit Card, please set up a regular bank transfer for your membership payment:</p>
            <blockquote>
              Name: Ballarat Hackerspace Inc<br/>
              BSB : 193-879<br/>
              Account No: 468 349 494<br/>
            </blockquote>
            <p>Please use the name we will know you as in the payment description.</p>
          </li>
          <li>Get down to the <a href="/about#space">space</a> and <em>hack</em> on stuff!</li>
        </ol>

        <h3>Not ready to become a Member?</h3>
        <p>If you're not ready to become a member but you want to be kept in the loop of things that are happening, or preparing to happen, at the space then just registering with our TidyHQ account will ensure you get all the pre-release information for new workshops and events.</p>
        <p>To sign up is even easier than becoming a member.</p>
        <ol>
          <li>Fill in the <a href="https://ballarathackerspace.tidyhq.com/signup/new">Registration Form</a> on TidyHQ.</li>
        </ol>
      </div>
    </div>
  </div>
</section>
