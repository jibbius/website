#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::TidyHQ;
use Mojo::Base 'Mojolicious::Plugin';

use Carp 'croak';
use Mojo::Collection qw(c);
use Mojo::JSON qw(encode_json);
use Mojo::Promise;
use Mojo::UserAgent;
use Mojo::Util qw(b64_encode dumper url_escape);
use Time::Piece;

our $VERSION = '0.1';

use constant {
  TIDYHQ_SESSION_KEY => 'tidyhq',
};

has _ua   => sub { Mojo::UserAgent->new };

sub register {
  my ($self, $app, $config) = @_;

  $app->log->warn('No TidyHQ organisation set!') unless $config->{organisation};

  my $url = Mojo::URL->new('https://api.tidyhq.com');
  my $url_org = Mojo::URL->new(sprintf 'https://%s.tidyhq.com', $config->{organisation} // '');

  $app->log->info('TidyHQ proxy registered for organisation: ' . ($config->{organisation} // '???'));

  $self->{url} = $url;
  $self->{url_org} = $url_org;
  $self->{config} = $config;

  $self->{proxy} = {
    members => {},
    tidyhq  => {}
  };

  # initial fetch of all users
  $self->proxy_users_get;

  $app->helper('tidyhq.contact.register' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    # required options
    my $name = $args->{name};
    my $email = $args->{email};
    my $interest = "RI: $args->{interest}";
    my $token = $self->proxy_auth_token;

    my ($tx, $err, $data, $contact, $group);

    # check if contact exists and create if not
    $tx = $self->_ua->get($url->path('/v1/contacts') => {Authorization => "Bearer $token"} => form => {search_terms => $email});

    ($contact, $err) = process_response($tx);
    $app->log->error("TidyHQ: Unable to search for existing contacts: $err") and return undef if $err;

    # more than one entry is an error
    $app->log->error("TidyHQ: Too many contacts found.") and return undef if @{$contact} > 1;

    # otherwise create contact
    if (@{$contact} == 0) {
      $tx = $self->_ua->post($url->path('/v1/contacts') => {Authorization => "Bearer $token"} => form => {first_name => $name, email_address => $email});
      ($contact, $err) = process_response($tx);

      $app->log->error("TidyHQ: Unable to create contact: $err") and return undef if $err;
    } else {
      $contact = $contact->[0];
    }

    # check if group exists and create if not
    $tx = $self->_ua->get($url->path('/v1/groups') => {Authorization => "Bearer $token"} => form => {search_terms => $interest});
    ($group, $err) = process_response($tx);
    $app->log->error("TidyHQ: Unable to search for existing interest groups: $err") and return undef if $err;

    # more than one entry is an error
    $app->log->error("TidyHQ: Too many interest groups found") and return undef if @{$group} > 1;

    # otherwise create group
    if (@{$group} == 0) {
      $tx = $self->_ua->post($url->path('/v1/groups') => {Authorization => "Bearer $token"} => form => {label => $interest});
      ($group, $err) = process_response($tx);
      $app->log->error("TidyHQ: Unable to create interest group: $err") and return undef if $err;
    } else {
      $group = $group->[0];
    }

    # add contact to group
    $tx = $self->_ua->put($url->path(sprintf('/v1/groups/%d/contacts/%d', $group->{id}, $contact->{id})) => {Authorization => "Bearer $token"});
    ($data, $err) = process_response($tx);

    $app->log->error("TidyHQ: Unable to add contact to interest group: $err") and return undef if $err;

    return !!1;
  });

  $app->helper('tidyhq.is_authenticated' => sub {
    my $c = shift;

    my $tidyhq = $c->session(TIDYHQ_SESSION_KEY) // {};

    # check token expiry
    if ($tidyhq->{token_expiry} && $tidyhq->{token_expiry} <= gmtime->epoch) {
      delete $c->session->{TIDYHQ_SESSION_KEY()};
      $tidyhq = {};
    }

    return $tidyhq->{id};
  });

  $app->helper('tidyhq.login' => sub {
    my $cb = ref $_[-1] eq 'CODE' ? pop : undef;
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $params = {
      client_id     => $config->{client_id},
      client_secret => $config->{client_secret},
      grant_type    => 'password',
      password      => $args->{password},
      username      => $args->{email},
    };

    my $promise = $self->_ua->post_p($url_org->path('/oauth/token'), form => $params)->then(sub {
      my $tx = shift;
      my ($data, $err) = process_response($tx);

      return Mojo::Promise->reject($err) if $err;

      return Mojo::Promise->all(
        $self->_ua->get_p($url->path('/v1/contacts/me') => {Authorization => "Bearer $data->{access_token}"}),
        Mojo::Promise->resolve($data->{access_token})
      );
    })->then(sub {
      my $tx = shift->[0];
      my $access_token = shift->[0];
      my ($data, $err) = process_response($tx);

      my $user = $self->proxy_user_get($data->{id});

      $user->{token}        = $access_token;
      $user->{token_expiry} = gmtime->epoch + 7200; # 2 hour token life

      # store all in session
      $c->session(TIDYHQ_SESSION_KEY() => $user);

      return Mojo::Promise->resolve($user);
    });

    return $promise;
  });

  $app->helper('tidyhq.logout' => sub {
    delete shift->session->{TIDYHQ_SESSION_KEY()};
  });

  $app->helper('tidyhq.shop.items' => sub {
    my $token = $self->proxy_auth_token;

    my $promise = $self->_ua->get_p($url->path('/v1/shop/products') => {Authorization => "Bearer $token"})->then(sub{
      my $tx = shift;

      my ($data, $err) = process_response($tx);

      if ($err) {
        $data = [];
        $app->log->error("TidyHQ: Unable to retrieve shop products: $err");
      }

      return Mojo::Promise->resolve($data);
    });

    return $promise;
  });

  $app->helper('tidyhq.sync' => sub {
    # re-fetch all users
    $self->proxy_users_get;
  });

  $app->helper('tidyhq.transactions' => sub {
    my $token = $self->proxy_auth_token;

    my $tx = $self->_ua->get($url->path('/v1/transactions') => {Authorization => "Bearer $token"});
    my ($data, $err) = process_response($tx);

    if ($err) {
      $data = [];
      $app->log->error("TidyHQ: Unable to retrieve transactions: $err");
    }

    # inflate date/times to Time::Piece objects
    my $balance = 0;

    for my $d (reverse @{$data}) {
      $d->{source} = 'TidyHQ';
      $balance += $d->{total};
      $d->{balance} = $balance;
      $d->{paid_at} = Time::Piece->strptime($d->{paid_at}, '%Y-%m-%dT%H:%M:%S+0000');
    }

    return $data;
  });

  $app->helper('tidyhq.users' => sub {
    return $self->{members};
  });
}

sub process_response {
  my $tx = shift;

  my ($data, $err);

  if ($err = $tx->error) {
    $err = $err->{message} || $err->{code};
  }
  elsif (!!$tx->res->headers->content_type && $tx->res->headers->content_type =~ m!^(application/json|text/javascript)(;\s*charset=\S+)?$!) {
    $data = $tx->res->json;
  }
  else {
    $data = Mojo::Parameters->new($tx->res->body)->to_hash;
  }

  # no data is an error in and of itself
  $err = $data ? '' : $err || 'Unknown error';

  return ($data, $err);
}

sub proxy_auth_token {
  my $self = shift;

  my $config = $self->{config};
  my $tidyhq = $self->{proxy}{tidyhq};
  my $url_org = $self->{url_org};

  if (! defined $tidyhq->{token_expiry} || $tidyhq->{token_expiry} <= gmtime->epoch) {
    my $params = {
      domain_prefix => $config->{organisation},
      client_id     => $config->{client_id},
      client_secret => $config->{client_secret},
      grant_type    => 'password',
      username      => $config->{proxy_email},
      password      => $config->{proxy_password},
    };

    my $tx = $self->_ua->post($url_org->path('/oauth/token'), form => $params);

    my ($data, $err) = process_response($tx);

    if ($err) {
      #$self->log->debug("proxy_auth_token() - error: $err");
      return undef;
    }

    # delay store token for final session store
    $self->{proxy}{tidyhq} = {
      token        => $data->{'access_token'},
      token_expiry => gmtime->epoch + 7200,  # 2 hour proxy token life
    };
  }

  return $self->{proxy}{tidyhq}{token};
}

sub proxy_users_get {
  my $self = shift;
  my $token = $self->proxy_auth_token;
  my $url = $self->{url};

  return unless $token;

  my ($data, $memberships, $contacts, $groups, $err);

  my $tx = $self->_ua->get($url->path('/v1/membership_levels') => {Authorization => "Bearer $token"});

  ($data, $err) = process_response($tx);

  my $membership_levels = {};
  map { $membership_levels->{$_->{id}} = lc $_->{name}} @{$data};

  $tx = $self->_ua->get($url->path('/v1/memberships') => {Authorization => "Bearer $token"});
  ($memberships, $err) = process_response($tx);

  # extract unique memberships
  $memberships = c(@{$memberships})->uniq(sub { return $_->{id}; });

  $tx = $self->_ua->get($url->path('/v1/contacts') => {Authorization => "Bearer $token"});
  ($contacts, $err) = process_response($tx);

  $tx = $self->_ua->get($url->path('/v1/groups') => {Authorization => "Bearer $token"});
  ($groups, $err) = process_response($tx);

  $data = [];

  # having a membership (active or not) indicates a member
  # collect unique id's
  my $members = { map { $_->{contact_id} => {meta => {}} } grep { $_->{contact_id} } @{$memberships->to_array} };

  for my $uid (keys %{$members}) {
    my $user = {
      id => $uid,
      memberships => [],
      subscriptions => []
    };

    # decorate contact
    my ($contact) = grep { $_->{id} eq $uid } @{$contacts};
    next unless $contact;
    $user->{contact} = $contact;

    # decorate memberships
    $memberships->grep(sub {
      my $m = shift;

      return !!$m->{contact_id} && $m->{contact_id} eq $uid;
    })->each(sub {
      my $m = shift;
      my $subscriptions_active = [grep { lc $_->{status} eq "active" } @{$m->{subscriptions}}];

      push @{$user->{memberships}}, {
        end_date => Time::Piece->strptime($m->{end_date}, '%Y-%m-%d'),
        name     => $membership_levels->{$m->{membership_level_id}},
        status   => (@{$subscriptions_active} > 0) ? 1 : 0,
        subscriptions => $m->{subscriptions},
      };
    });

    $self->{members}{$uid} = $user;
  }

  return $data;
}

sub proxy_user_get {
  my $self = shift;
  my $uid  = shift;

  return undef unless $uid;

  my $url = $self->{url};
  my $user = $self->{members}{$uid};

  my $now = gmtime;

  # update user as appropriate
  unless ($user) {
    my $token = $self->proxy_auth_token;

    my $headers = {Authorization => "Bearer $token"};

    my $tx = $self->_ua->get($url->path("/v1/contacts/$uid") => $headers);
    my ($data, $err) = process_response($tx);

    return undef if $err;

    $user = {
      id      => $data->{id},
      contact => $data,
      updated => $now
    };

    # fetch and process user's groups
    my $path = sprintf '/v1/contacts/%d/groups', $data->{id};
    $tx = $self->_ua->get($url->path($path) => $headers);
    ($data, $err) = process_response($tx);

    # store all groups (lowercase'd)
    $user->{contact}{groups} = [map { lc $_->{label} } @{$data}];

    # fetch and process current membership-levels
    $tx = $self->_ua->get($url->path('/v1/membership_levels') => $headers);
    ($data, $err) = process_response($tx);

    return undef if $err;

    my $membership_levels = {};
    map { $membership_levels->{$_->{id}} = lc $_->{name}} @{$data};

    # fetch and process users's memberships
    $path = sprintf '/v1/contacts/%d/memberships', $uid;
    $tx = $self->_ua->get($url->path($path) => $headers);
    ($data, $err) = process_response($tx);

    # extract unique memberships
    my $api_memberships = c(@{$data})->uniq(sub { return $_->{id}; });

    # check we are in the group label "Members"
    if (!$err && $api_memberships->size) {
      my $memberships = [];

      $api_memberships->each(sub {
        my $m = shift;
        my $subscriptions_active = [grep { lc $_->{status} eq "active" } @{$m->{subscriptions}}];

        push @{$memberships}, {
          end_date => Time::Piece->strptime($m->{end_date}, '%Y-%m-%d'),
          name     => $membership_levels->{$m->{membership_level_id}},
          status   => (@{$subscriptions_active} > 0) ? 1 : 0,
          subscriptions => $m->{subscriptions},
        };
      });

      # add groups to data and store all in session
      $user->{memberships} = $memberships;

      # store for later
      $self->{members}{$uid} = $user;
    }
  }

  return $user;
}

1;
